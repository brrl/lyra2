# node-lyra2

[![Build Status](https://travis-ci.org/abrkn/node-lyra2.svg?branch=master)](https://travis-ci.org/abrkn/node-lyra2)

Lyra2 addon for Node.js

## Installing

```bash
npm install lyra2
```

## Testing

```bash
npm test
```

## Author of bindings

Andreas Brekken <andreas@brekken.com>

## License

ISC
